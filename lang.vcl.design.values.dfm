object frmValues: TfrmValues
  Left = 0
  Top = 0
  BorderIcons = [biMaximize]
  Caption = 'Values for ""'
  ClientHeight = 336
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    635
    336)
  PixelsPerInch = 96
  TextHeight = 13
  object vleValues: TValueListEditor
    Left = 8
    Top = 8
    Width = 619
    Height = 289
    Anchors = [akLeft, akTop, akRight, akBottom]
    KeyOptions = [keyEdit, keyAdd, keyDelete, keyUnique]
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goEditing, goTabs, goAlwaysShowEditor, goThumbTracking]
    TabOrder = 0
    TitleCaptions.Strings = (
      'Language'
      'Text')
    ColWidths = (
      150
      463)
    RowHeights = (
      18
      18)
  end
  object btnOK: TButton
    Left = 8
    Top = 303
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
  end
  object btnCancel: TButton
    Left = 552
    Top = 303
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
