unit lang.design.stringsframe;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.ValEdit,
  ToolsAPI, Vcl.ActnList, VCL.ImgList, VCL.Menus, VCL.ComCtrls, IniFiles,
  DesignIntf, Vcl.Clipbrd, lang.manager, Vcl.ExtCtrls, Vcl.StdCtrls,
  System.Generics.Collections;

type
  TfrmDefaultStrings = class(TFrame)
    vleStrings: TValueListEditor;
    tmrUpdate: TTimer;
    StatusBar: TStatusBar;
    cbLangs: TComboBox;
    txtLog: TMemo;
    procedure tmrUpdateTimer(Sender: TObject);
    procedure ResetTimer(Sender: TObject);
    procedure vleStringsSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure vleStringsSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure vleStringsGetEditText(Sender: TObject; ACol, ARow: Integer;
      var Value: string);
    procedure vleStringsExit(Sender: TObject);
    procedure vleStringsKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure vleStringsDblClick(Sender: TObject);
    procedure StatusBarClick(Sender: TObject);
    procedure cbLangsChange(Sender: TObject);
    procedure cbLangsExit(Sender: TObject);
  private
    FModules: IOTAModuleServices;
    FLang : TLangManager;
    FForm : TComponent;
    FLastFile: String;
    FLastValue : string;
    FFileLangs : TDictionary<string, TLangManager>;
    FOnDestroying: TProc<TObject>;
    procedure SetLang(const Value: TLangManager);
    property Lang : TLangManager read FLang write SetLang;
    procedure NewValueFromClipboard;
    procedure NewValueFromEditor;
    procedure SaveID(const Value: string);
    procedure SaveValue(const ID, Value : string);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
  public
    property OnDestroying : TProc<TObject> read FOnDestroying write FOnDestroying;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

  TDockableLangStrings = class(TInterfacedObject, INTACustomDockableForm)
  private
    FOnDestroying: TProc<TObject>;
  public
    constructor Create(OnDestroying : TProc<TObject>);
    { Returns the Caption for the Dockable Form }
    function GetCaption: string;
    { Returns a unique identifier for this form.  This should not be translated.
      This identifier is used as the section name when saving information for
      this form in the desktop state file }
    function GetIdentifier: string;
    { Returns the class of the frame that you want embedded in the dockable form }
    function GetFrameClass: TCustomFrameClass;
    { Called when an instance of the specified frame class is created }
    procedure FrameCreated(AFrame: TCustomFrame);
    { Returns an action list that is used to populate the form's context menu.
      By default the context menu will have 2 items that are common to all
      dockable forms in the IDE: "Stay on top" and "Dockable".  If the form
      has a toolbar, there will also be a "Toolbar" menu item.  If this
      function returns a non-nil action list, the items in the action list will
      be added to the menu (above the default items).  To specify sub-menus, use
      categories for the actions contained in the Action List.  Any action that
      has a Category set, will appear on a sub-menu in the context menu.  The
      Caption of the Parent menu will be the Category name. }
    function GetMenuActionList: TCustomActionList;
    { Returns an image list that contains the images associated with the action
      list returned by GetMenuActionList }
    function GetMenuImageList: TCustomImageList;
    { Called when the popup menu is about to be shown.  This allows further
      customization beyond just adding items from an Action List }
    procedure CustomizePopupMenu(PopupMenu: TPopupMenu);
    { Returns an action list that is used to populate a toolbar on the form.  If
      nil is returned, then the dockable form will not have a toolbar.  Items in
      the Action List that have '-' as the caption will be added to the toolbar
      as a separator }
    function GetToolBarActionList: TCustomActionList;
    { Returns an image list that contains the images associated with the action
      list returned by GetToolbarActionList }
    function GetToolBarImageList: TCustomImageList;
    { Called after the toolbar has been populated with the Action List returned
      from GetToolbarActionList.  This allows further customization beyond just
      adding items from an Action List }
    procedure CustomizeToolBar(ToolBar: TToolBar);
    { Called when state for this form is saved to a desktop file.  The Section
      paramter is passed in for convenience, but it should match the string
      returned by GetIdentifier.  This is only called for INTACustomDockableForm
      instances that have been registered using INTAServices.RegisterDockableForm.
      IsProject indicates whether the desktop being saved is a project desktop
      (as opposed to a dekstop state) }
    procedure SaveWindowState(Desktop: TCustomIniFile; const Section: string; IsProject: Boolean);
    { Called when state for this form is loaded from a desktop file.  The
      Section paramter is passed in for convenience, but it should match the
      string returned by GetIdentifier.  This is only called for
      INTACustomDockableForm instances that have been registered using
      INTAServices.RegisterDockableForm }
    procedure LoadWindowState(Desktop: TCustomIniFile; const Section: string);
    { Allows the form to control the enabled state of the clipboard commands on
      the IDE's "Edit" menu when this view is active }
    function GetEditState: TEditState;
    { Called when the user uses one of the clipboard commands on the IDE's "Edit"
      menu }
    function EditAction(Action: TEditAction): Boolean;

    property Caption: string read GetCaption;
    property Identifier: string read Getidentifier;
    property FrameClass: TCustomFrameClass read GetFrameClass;
    property MenuActionList: TCustomActionList read GetMenuActionList;
    property MenuImageList: TCustomImageList read GetMenuImageList;
    property ToolbarActionList: TCustomActionList read GetToolbarActionList;
    property ToolbarImageList: TCustomImageList read GetToolbarImageList;
  end;

  TKeyboardBinding = class(TNotifierObject, IOTAKeyboardBinding)
  private
    Procedure ExtractTextToLangMan(const Context: IOTAKeyContext;
      KeyCode: TShortcut; var BindingResult: TKeyBindingResult);
  protected
    procedure BindKeyboard(const BindingServices: IOTAKeyBindingServices);
    function GetBindingType: TBindingType;
    function GetDisplayName: string;
    function GetName: string;
  end;

implementation

uses System.RTTI, duck;

{$R *.dfm}

var _StringTool : TfrmDefaultStrings;

{ TDockableLangStrings }

constructor TDockableLangStrings.Create(OnDestroying: TProc<TObject>);
begin
  inherited Create;
  FOnDestroying := OnDestroying;
end;

procedure TDockableLangStrings.CustomizePopupMenu(PopupMenu: TPopupMenu);
begin

end;

procedure TDockableLangStrings.CustomizeToolBar(ToolBar: TToolBar);
begin

end;

function TDockableLangStrings.EditAction(Action: TEditAction): Boolean;
begin
  Result := False;
  case Action of
    eaUndo: ;
    eaRedo: ;
    eaCut: ;
    eaCopy:
      ;//Clipboard.AsText := FLangName+'.Strings[''ID'']';
    eaPaste: ;
    eaDelete: ;
    eaSelectAll: ;
    eaPrint: ;
    eaElide: ;
  end;
end;

procedure TDockableLangStrings.FrameCreated(AFrame: TCustomFrame);
begin
  TfrmDefaultStrings(AFrame).OnDestroying :=
    procedure(Sender : TObject)
    begin
      FOnDestroying(Sender);
    end;
end;

function TDockableLangStrings.GetCaption: string;
begin
  Result := 'LangManager Strings';
end;

function TDockableLangStrings.GetEditState: TEditState;
begin
  Result := [esCanUndo, esCanRedo, esCanCut, esCanCopy, esCanPaste,
    esCanDelete, esCanEditOle, esCanPrint, esCanSelectAll, esCanCreateTemplate,
    esCanElide];
end;

function TDockableLangStrings.GetFrameClass: TCustomFrameClass;
begin
  Result := TfrmDefaultStrings;
end;

function TDockableLangStrings.GetIdentifier: string;
begin
  Result := 'LangMan';
end;

function TDockableLangStrings.GetMenuActionList: TCustomActionList;
begin
  Result := nil;
end;

function TDockableLangStrings.GetMenuImageList: TCustomImageList;
begin
  Result := nil;
end;

function TDockableLangStrings.GetToolBarActionList: TCustomActionList;
begin
  Result := nil;
end;

function TDockableLangStrings.GetToolBarImageList: TCustomImageList;
begin
  Result := nil;
end;

procedure TDockableLangStrings.LoadWindowState(Desktop: TCustomIniFile;
  const Section: string);
begin

end;

procedure TDockableLangStrings.SaveWindowState(Desktop: TCustomIniFile;
  const Section: string; IsProject: Boolean);
begin

end;

{ TfrmDefaultStrings }

procedure TfrmDefaultStrings.cbLangsChange(Sender: TObject);
begin
  if cbLangs.ItemIndex >= 0 then
  begin
    Lang := TLangManager(cbLangs.Items.Objects[cbLangs.ItemIndex]);
  end else
    Lang := nil;
  cbLangs.Visible := False;
  StatusBar.Visible := True;
end;

procedure TfrmDefaultStrings.cbLangsExit(Sender: TObject);
begin
  cbLangs.Visible := False;
  StatusBar.Visible := True;
end;

constructor TfrmDefaultStrings.Create(AOwner: TComponent);
begin
  inherited;
  FModules := (BorlandIDEServices as IOTAModuleServices);
  FFileLangs := TDictionary<string, TLangManager>.Create;
  _StringTool := Self;
end;

destructor TfrmDefaultStrings.Destroy;
begin
  if Assigned(FOnDestroying) then
    FOnDestroying(Self);
  FFileLangs.Free;
  _StringTool := nil;
  inherited;
end;

procedure TfrmDefaultStrings.SaveID(const Value: string);
var
  idx : integer;
  sID: string;
begin
  if FLang = nil then
    exit;
  sID := Value;
  if vleStrings.Values[sID] <> '' then
  begin
    if FLastValue <> '' then
    begin
      idx := FLang.DefaultStrings.IndexOfName(FLastValue);
      if idx >= 0 then
        FLang.DefaultStrings.Delete(idx);
    end;
    FLang.DefaultStrings.Values[sID] := Value;
  end;
end;

procedure TfrmDefaultStrings.SaveValue(const ID, Value: string);
var
  idx : integer;
  i: Integer;
begin
  if (FLang = nil) or (ID = '') then
    exit;
  idx := FLang.DefaultStrings.IndexOfName(ID);
  if (Value = '') and (idx >= 0) then
  begin
    FLang.DefaultStrings.Delete(idx);
  end else
  begin
    FLang.DefaultStrings.Values[ID] := Value;
  end;
  for i := 0 to FModules.CurrentModule.ModuleFileCount-1 do
    FModules.CurrentModule.ModuleFileEditors[i].MarkModified;
end;

procedure TfrmDefaultStrings.NewValueFromClipboard;
var
  sID : string;
  se : IOTASourceEditor;
  sText : string;
  i : integer;
begin
  sText := Clipboard.AsText;
  if sText.StartsWith('''') and sText.EndsWith('''') then
    sText := sText.Substring(1,sText.Length-2);
  if (sText = '') or (Lang = nil) then
    exit;

  if InputQuery('New String', 'Enter ID', sID) or (sID = '') then
  begin
    if vleStrings.FindRow(sID, i) then
    begin
      if MessageDlg('The ID "'+sID+'" already exists.'#13#10#13#10'Would you like to use the value already mapped?',TMsgDlgType.mtConfirmation, mbYesNo, 0, mbNo) = mrYes then
      begin
        se.Show;
        se.EditViews[0].Buffer.EditPosition.InsertText(Lang.Name+'['''+sID+''']');
        SaveValue(sID, sText);
      end;
      exit;
    end;

    vleStrings.InsertRow(sID, sText, True);
    if FModules.CurrentModule.CurrentEditor.QueryInterface(IOTASourceEditor, se) = 0 then
    begin
      se.Show;
      se.EditViews[0].Buffer.EditPosition.InsertText(Lang.Name+'['''+sID+''']');
    end;
    SaveValue(sID, sText);
  end;
end;

procedure TfrmDefaultStrings.NewValueFromEditor;
var
  sID : string;
  se : IOTASourceEditor;
  sText : string;
  i : integer;
begin
  if FModules.CurrentModule.CurrentEditor.QueryInterface(IOTASourceEditor, se) <> 0 then
    exit;
  sText := se.EditViews[0].Buffer.EditBlock.Text;
  if sText.StartsWith('''') and sText.EndsWith('''') then
    sText := sText.Substring(1,sText.Length-2);
  if (sText = '') or (Lang = nil) then
    exit;

  for i := FLang.DefaultStrings.Count-1 downto 0 do
  begin
    if FLang.DefaultStrings.ValueFromIndex[i] = sText then
    begin
      se.Show;
      se.EditViews[0].Buffer.EditPosition.InsertText(Lang.Name+'['''+FLang.DefaultStrings.Names[i]+''']');
      SaveValue(sID, sText);
      exit;
    end;
  end;

  if InputQuery('New String', 'Enter ID', sID) or (sID = '') then
  begin
    if vleStrings.FindRow(sID, i) then
    begin
      if MessageDlg('The ID "'+sID+'" already exists.'#13#10#13#10'Would you like to use the value already mapped?',TMsgDlgType.mtConfirmation, mbYesNo, 0, mbNo) = mrYes then
      begin
        se.Show;
        se.EditViews[0].Buffer.EditPosition.InsertText(Lang.Name+'['''+sID+''']');
        SaveValue(sID, sText);
      end;
      exit;
    end;
    vleStrings.InsertRow(sID, sText, True);
    se.Show;
    se.EditViews[0].Buffer.EditPosition.InsertText(Lang.Name+'['''+sID+''']');
    SaveValue(sID, sText);
  end;
end;

procedure TfrmDefaultStrings.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if (Operation = TOperation.opRemove) and (AComponent = FLang) then
    Lang := nil;
end;

procedure TfrmDefaultStrings.ResetTimer(Sender: TObject);
begin
  tmrUpdate.Enabled := False;
  tmrUpdate.Enabled := True;
end;

procedure TfrmDefaultStrings.SetLang(const Value: TLangManager);
var
  i : integer;
  iRow : integer;
begin
  FLang := Value;
  if FLang <> nil then
  begin
    FLang.FreeNotification(Self);
    FForm := FLang.Root
  end else
    FForm := nil;

  if FLang <> nil then
  begin
    for i := 0 to FLang.DefaultStrings.Count -1 do
      if vleStrings.FindRow(FLang.DefaultStrings.Names[i], iRow) then
      begin
        vleStrings.Values[FLang.DefaultStrings.Names[i]] := FLang.DefaultStrings.ValueFromIndex[i];
      end else
      begin
        vleStrings.InsertRow(FLang.DefaultStrings.Names[i], FLang.DefaultStrings.ValueFromIndex[i],True);
      end;
    vleStrings.Enabled := True;
    if FForm <> nil then
      StatusBar.SimpleText := FForm.Name+'.'+FLang.Name
    else
      StatusBar.SimpleText := '(global).'+FLang.Name;
  end else
  begin
    StatusBar.SimpleText := 'No TLangManager Found';
    vleStrings.Strings.Clear;
    vleStrings.Enabled := False;
  end;

end;

procedure TfrmDefaultStrings.StatusBarClick(Sender: TObject);
var
  ary : TArray<TPair<String, TLangManager>>;
  a: TPair<String, TLangManager>;
begin
  cbLangs.Items.BeginUpdate;
  try
    cbLangs.Items.Clear;
    ary := FFileLangs.ToArray;
    for a in ary do
    begin
      if a.Value = nil then
        Continue;
      if cbLangs.Items.IndexOfObject(a.Value) < 0 then
      begin
        if a.Value.Root <> nil then
          cbLangs.AddItem(a.Value.Root.Name+'.'+a.Value.Name, a.Value)
        else
          cbLangs.AddItem('(global).'+a.Value.Name, a.Value);
      end;
    end;
  finally
    cbLangs.Items.EndUpdate;
  end;
  if Lang <> nil  then
    if Lang.Root <> nil then
      cbLangs.ItemIndex := cbLangs.Items.IndexOf(Lang.Root.Name+'.'+Lang.Name)
    else
      cbLangs.ItemIndex := cbLangs.Items.IndexOf('(global).'+Lang.Name)
  else
    cbLangs.ItemIndex := -1;
  cbLangs.Visible := True;
  StatusBar.Visible := False;
  cbLangs.DroppedDown := True;
end;

procedure TfrmDefaultStrings.tmrUpdateTimer(Sender: TObject);
var
  fe : IOTAFormEditor;
  se : IOTASourceEditor;
  i : integer;
  lmLang : TLangManager;
  slFiles : TStringList;
  clsForm, clsModule : TClass;
  cmpDesignerForm : TComponent;
begin
  lmLang := nil;

  txtLog.Lines.BeginUpdate;
  tmrUpdate.Enabled := False;
  try
    if (FModules.CurrentModule <> nil) and (FModules.CurrentModule.CurrentEditor <> nil) then
    begin
      slFiles := TStringList.Create;
      try
        FModules.CurrentModule.GetAssociatedFilesFromModule(slFiles);
        if FModules.CurrentModule.CurrentEditor.QueryInterface(IOTAFormEditor, fe) = 0 then
        begin
          clsForm := GetClass('TLanguageForm');
          clsModule := GetClass('TLanguageModule');
          cmpDesignerForm := TComponent(fe.GetRootComponent.GetComponentHandle);
          if cmpDesignerForm = nil then
            exit;
          if (cmpDesignerForm is clsForm) or
             (cmpDesignerForm is clsModule) or
             cmpDesignerForm.duck.has('Lang') then
          begin
            if (cmpDesignerForm is clsForm) then
              txtLog.Lines.Insert(0, 'Designer Form is '''+cmpDesignerForm.ClassName+''' and is a TLanguageForm')
            else if (cmpDesignerForm is clsModule) then
              txtLog.Lines.Insert(0, 'Designer Form is '''+cmpDesignerForm.ClassName+''' and is a TLanguageModule')
            else if cmpDesignerForm.duck.has('Lang') then
              txtLog.Lines.Insert(0, 'Designer Form is '''+cmpDesignerForm.ClassName+''' and has a Lang property');
            lmLang := TLangManager(cmpDesignerForm.duck.get('Lang', TLangManager).AsObject);
            FForm :=  TComponent(fe.GetRootComponent.GetComponentHandle);
          end else
          begin
            for i := 0 to fe.GetRootComponent.GetComponentCount-1 do
            begin
              if fe.GetRootComponent.GetComponent(i).GetComponentType = 'TLangManager' then
              begin
                lmLang := TLangManager(fe.GetRootComponent.GetComponent(i).GetComponentHandle);
                FForm := lmLang.Root;
                break;
              end;
            end;

            FForm := nil;
          end;

          if lmLang = FLang then
            exit;
          txtLog.Lines.Insert(0, 'Changing Lang Managers');
          Lang := lmLang;

          for i := 0 to slFiles.Count-1 do
          begin
            FFileLangs.AddOrSetValue(slFiles[i], Lang);
          end;
        end else if FModules.CurrentModule.CurrentEditor.QueryInterface(IOTASourceEditor, se) = 0 then
        begin
          if (FLastFile <> FModules.CurrentModule.CurrentEditor.FileName) then
          begin
            if not FFileLangs.ContainsKey(FModules.CurrentModule.CurrentEditor.FileName) then
            begin
              FLastFile := FModules.CurrentModule.CurrentEditor.FileName;
              for i := 0 to se.GetSubViewCount-1 do
                if se.GetSubViewIdentifier(i) = 'Borland.FormDesignerView' then
                begin
                  txtLog.Lines.Insert(0, 'Found Designer for Editor');
                  se.SwitchToView(i);
                  exit;
                end;
              Lang := nil;
            end else
            begin
              Lang := FFileLangs[FModules.CurrentModule.CurrentEditor.FileName];
            end;
          end;
        end;
      finally
        slFiles.Free;
      end;
    end else
    begin
      txtLog.Lines.Insert(0, 'No module or editor found.');
      Lang := nil;
    end;
    while txtLog.Lines.Count > 50 do
      txtLog.Lines.Delete(txtLog.Lines.Count-1);
  finally
    tmrUpdate.Enabled := True;
    txtLog.Lines.EndUpdate;
  end;
end;

procedure TfrmDefaultStrings.vleStringsDblClick(Sender: TObject);
begin
  NewValueFromClipboard;
end;

procedure TfrmDefaultStrings.vleStringsExit(Sender: TObject);
begin
  ResetTimer(nil);
end;

procedure TfrmDefaultStrings.vleStringsGetEditText(Sender: TObject; ACol,
  ARow: Integer; var Value: string);
begin
  tmrUpdate.Enabled := False;
  FLastValue := Value;
end;

procedure TfrmDefaultStrings.vleStringsKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Shift = [ssCtrl]) and ((Key = VK_INSERT) or (Key = Ord('V')) or (Key = Ord('v'))) then
    NewValueFromClipboard;
  if (Shift = [ssCtrl]) and ((Key = VK_ESCAPE) or (Key = Ord('L')) or (Key = Ord('l'))) then
    txtLog.Visible := not txtLog.Visible;
end;

procedure TfrmDefaultStrings.vleStringsSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  ResetTimer(Sender);
end;

procedure TfrmDefaultStrings.vleStringsSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
var
  sID : string;
begin
  if Value = FLastValue then
  begin
    FLastValue := '';
    exit;
  end;
  try
    if ACol = 0 then
    begin
      SaveID(Value);
    end else if ACol = 1 then
    begin
      sID := vleStrings.Keys[ARow];
      if (sID = '') and (Value = '') then
        exit;
      if sID = '' then
        if (not InputQuery('New String', 'Enter ID', sID)) or (sID = '') then
          exit;
      SaveValue(sID, Value);
    end;
  finally
    ResetTimer(nil);
  end;
end;

{ TKeyboardBinding }

procedure TKeyboardBinding.BindKeyboard(
  const BindingServices: IOTAKeyBindingServices);
begin
  BindingServices.AddKeyBinding([TextToShortcut('Ctrl+Alt+C')], ExtractTextToLangMan, Nil);
end;

procedure TKeyboardBinding.ExtractTextToLangMan(const Context: IOTAKeyContext;
  KeyCode: TShortcut; var BindingResult: TKeyBindingResult);
begin
  if Assigned(_StringTool) and (Context.EditBuffer.EditBlock.Size > 1) then
  begin
    _StringTool.NewValueFromEditor;
    BindingResult := krHandled;
  end else
    BindingResult := krNextProc;
end;

function TKeyboardBinding.GetBindingType: TBindingType;
begin
  Result := TBindingType.btPartial;
end;

function TKeyboardBinding.GetDisplayName: string;
begin
  Result := 'Extract String for Translation';
end;

function TKeyboardBinding.GetName: string;
begin
  Result := 'LangManExtract';
end;

initialization
  _StringTool := nil;

end.
