unit lang.vcl.design.editor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, lang.manager,
  System.Generics.Collections;


type
  TfrmLangEditor = class(TForm)
    cbDesign: TComboBox;
    Label1: TLabel;
    cbSelected: TComboBox;
    Label2: TLabel;
    tvEditor: TTreeView;
    btnOK: TButton;
    btnCancel: TButton;
    btnAddString: TButton;
    procedure cbDesignExit(Sender: TObject);
    procedure cbSelectedExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure tvEditorDblClick(Sender: TObject);
    procedure btnAddStringClick(Sender: TObject);
  private
    type
      TLangEditorLauncher = class(TInterfacedObject, ILangEditor)
      public
        function Edit(Lang: TLangManager): Boolean;
      end;
    class var FLangEditorLauncher : TLangEditorLauncher;
  private
    FLang: TLangManager;
    FComponentNodes : TDictionary<TComponent, TTreeNode>;

    procedure SetLang(const Value: TLangManager);
    class procedure RegisterEditor;
  public
    class function Edit(Lang : TLangManager) : TModalResult;
    property Lang : TLangManager read FLang write SetLang;
  end;

implementation

uses System.RTTI, System.TypInfo, Vcl.Menus, lang.vcl.design.values, chimera.json;

{$R *.dfm}

{ TfrmLangEditor }

procedure TfrmLangEditor.btnAddStringClick(Sender: TObject);
var
  sID : string;
  i : integer;
  tnStrings, tn : TTreeNode;
begin
  if InputQuery('Add String', 'New String ID', sID) then
  begin
    tnStrings := nil;
    for i := 0 to tvEditor.Items.Count -1 do
    begin
      if tvEditor.Items[i].Text = '~strings' then
      begin
        tnStrings := tvEditor.Items[i];
        break;
      end;
    end;
    if tnStrings = nil then
      tnStrings := tvEditor.Items.Add(nil,'~strings');
    tn := tvEditor.Items.AddChild(tnStrings, sID+' = ""');
    FLang.StringValues[sID] := TfrmValues.ValueEdit(sID, FLang.SupportedLangs,JSON());
    //tn.Text := sID+' = '+FLang.Strings[sID];
  end;
end;

procedure TfrmLangEditor.cbDesignExit(Sender: TObject);
begin
  if cbDesign.Items.IndexOf(cbDesign.Text) < 0 then
    cbDesign.Items.Add(cbDesign.Text);
  cbSelected.Items.Assign(cbDesign.Items);
end;

procedure TfrmLangEditor.cbSelectedExit(Sender: TObject);
begin
  if cbSelected.Items.IndexOf(cbSelected.Text) < 0 then
    cbSelected.Items.Add(cbSelected.Text);
  cbDesign.Items.Assign(cbSelected.Items);
end;

class function TfrmLangEditor.Edit(Lang: TLangManager): TModalResult;
var
  frm : TfrmLangEditor;
begin
  frm := TfrmLangEditor.Create(nil);
  try
    frm.Lang := Lang;
    frm.cbDesign.Items.Assign(Lang.SupportedLangs);
    frm.cbSelected.Items.Assign(Lang.SupportedLangs);
    Result := frm.ShowModal;
    if Result = mrOK then
    begin
      frm.Lang.SupportedLangs.Assign(frm.cbDesign.Items);
      frm.Lang.DesignLang := frm.cbDesign.Text;
      frm.Lang.SelectedLang:= frm.cbSelected.Text;
      frm.Lang.SaveFile;
    end;
  finally
    frm.Free;
  end;
end;

procedure TfrmLangEditor.FormCreate(Sender: TObject);
begin
  FComponentNodes := TDictionary<TComponent, TTreeNode>.Create;
end;

procedure TfrmLangEditor.FormDestroy(Sender: TObject);
begin
  FComponentNodes.Free;
end;

class procedure TfrmLangEditor.RegisterEditor;
begin
  //if csDesigning in ComponentState then
  //  exit;
  FLangEditorLauncher := TLangEditorLauncher.Create;
  TLangManager.Editor := FLangEditorLauncher;
end;

procedure TfrmLangEditor.SetLang(const Value: TLangManager);
  procedure IterateComponents(cmp : TComponent; ParentNode : TTreeNode);
    procedure IterateProperties(cmp : TComponent; ParentNode : TTreeNode);
    var
      cxt : TRTTIContext;
      t : TRTTIType;
      p : TRTTIProperty;
    begin
      t := cxt.GetType(cmp.ClassType);
      for p in t.GetProperties do
      begin
        if p.PropertyType.TypeKind in [
            tkString, tkWideString, tkChar, tkWideChar,
            tkLString, tkWString, tkUString, tkAnsiChar,
            tkUnicodeString, tkAnsiString, tkShortString] then
        begin
          if (p.Name = 'Name') or (p.Name = 'EngineVersion') or (P.Visibility <> TMemberVisibility.mvPublished)  then
            Continue;
          if FLang.SelectedLang = FLang.DesignLang then
            FLang.Properties[cmp,p.Name] := p.GetValue(cmp).AsString;
          tvEditor.Items.AddChild(ParentNode, p.Name+' = "'+FLang.Properties[cmp,p.Name]);
        end;
      end;
    end;
  var
    i : integer;
    tn : TTreeNode;
  begin
    for i := 0 to cmp.ComponentCount-1 do
    begin
      if cmp.Components[i] <> FLang then
      begin
        tn := tvEditor.Items.AddChildObject(ParentNode, cmp.Components[i].Name + ' : '+cmp.Components[i].ClassName, cmp.Components[i]);
        FComponentNodes.Add( cmp.Components[i], tn);
        IterateProperties(cmp.Components[i], tn);
        IterateComponents(cmp.Components[i], tn);
      end;
    end;
  end;
  procedure ReparentNodes;
    procedure ChangeNodeParent(tn : TTreeNode; NewParent : TTreeNode);
    var
      tnNew, tnItem : TTreeNode;
      i: Integer;
      cmp : TComponent;
    begin
      cmp := TComponent(tn.Data);
      tnNew := tvEditor.Items.AddChildObjectFirst(NewParent, cmp.Name+' : '+cmp.ClassName, cmp);
      FComponentNodes.AddOrSetValue(cmp, tnNew);
      for i := tn.Count-1 downto 0 do
      begin
        if tn.Item[i].Data <> nil then
        begin
          ChangeNodeParent(tn.Item[i], tnNew);
        end else
        begin
          tnItem := tvEditor.Items.AddChildFirst(tnNew, tn.Item[i].Text);
        end;
      end;
      tvEditor.Items.Delete(tn);
    end;
  var
    cmp : TComponent;
    i : integer;
  begin
    for i := 0 to tvEditor.Items.Count - 1 do
    begin
      if tvEditor.Items[i].Data = nil then
        Continue;
      cmp := TComponent(tvEditor.Items[i].Data);
      if cmp is TControl then
      begin
        if TControl(cmp).Parent <> FLang.Root then
        begin
          ChangeNodeParent(tvEditor.items[i], FComponentNodes[TControl(cmp).Parent]);
        end;
      end else if cmp is TMenuItem then
      begin
        if TMenuItem(cmp).Parent <> nil then
        begin
          ChangeNodeParent(tvEditor.items[i], FComponentNodes[TMenuItem(cmp).Parent]);
        end;
      end;
    end;
  end;
var
  tnStrings : TTreeNode;
begin
  FLang := Value;
  cbDesign.Items.Assign(FLang.SupportedLangs);
  cbSelected.Items.Assign(FLang.SupportedLangs);
  cbDesign.Text := FLang.DesignLang;
  cbSelected.Text := FLang.SelectedLang;
  FComponentNodes.Clear;
  tvEditor.Items.Clear;

  IterateComponents(FLang.Root, nil);
  ReparentNodes;
  tnStrings := tvEditor.Items.Add(nil, '~strings');
  FLang.EachID(
    procedure(const ID : string; const Obj : IJSONObject)
    var
      s : string;
    begin
      if Obj.Has[FLang.SelectedLang] then
        s := Obj.Strings[FLang.SelectedLang]
      else
        s := '';
      tvEditor.Items.AddChild(tnStrings, ID+' = '+s);
    end
  );
end;

procedure TfrmLangEditor.tvEditorDblClick(Sender: TObject);
var
  sProp : string;
begin
  if (tvEditor.Selected <> nil) and (tvEditor.Selected.Parent <> nil) and (tvEditor.Selected.Parent.Text = '~strings') then
  begin
    sProp := tvEditor.Selected.Text.Split(['='])[0].Trim;
    FLang.StringValues[sProp] := TfrmValues.ValueEdit(sProp, FLang.SupportedLangs, FLang.StringValues[sProp]);
  end else if (tvEditor.Selected <> nil) and (tvEditor.Selected.Data = nil) and (tvEditor.Selected.Parent <> nil) then
  begin
    sProp := tvEditor.Selected.Text.Split(['='])[0].Trim;
    FLang.Values[tvEditor.Selected.Parent.Data, sProp] := TfrmValues.ValueEdit(tvEditor.Selected.Parent.Data, sProp, FLang.SupportedLangs, FLang.Values[tvEditor.Selected.Parent.Data, sProp]);
  end;
  cbSelected.Items.Assign(FLang.SupportedLangs);
  cbDesign.Items.Assign(FLang.SupportedLangs);
end;

{ TfrmLangEditor.TLangEditorLauncher }

function TfrmLangEditor.TLangEditorLauncher.Edit(Lang: TLangManager): Boolean;
begin
  Result := TfrmLangEditor.Edit(Lang) = mrOK;
end;

initialization
  TfrmLangEditor.RegisterEditor;


end.
