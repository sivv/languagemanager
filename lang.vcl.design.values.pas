unit lang.vcl.design.values;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.ValEdit, chimera.json,
  Vcl.StdCtrls;

type
  TfrmValues = class(TForm)
    vleValues: TValueListEditor;
    btnOK: TButton;
    btnCancel: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FDefaultStrings : TStringList;
    procedure ConfigureForStrings(DefaultStrings : TStrings);
    procedure UpdateDefaultStrings(Strings : TStrings);
  public
    class function ValueEdit(cmp : TComponent; const Prop : string; SupportedLangs : TStrings; Current : IJSONObject) : IJSONObject; overload;
    class function ValueEdit(const ID : string; SupportedLangs : TStrings; Current : IJSONObject) : IJSONObject; overload;
    class procedure DefaultStringsEdit(DefaultStrings : TStrings);
  end;

implementation

{$R *.dfm}

{ TfrmValues }

class function TfrmValues.ValueEdit(cmp : TComponent; const Prop : string; SupportedLangs: TStrings;
  Current: IJSONObject): IJSONObject;
var
  frm : TfrmValues;
  i: Integer;
  s : string;
begin
  frm := TfrmValues.Create(nil);
  try
    frm.Caption := 'Values for '+cmp.Name+'.'+Prop;
    for i := 0 to SupportedLangs.Count-1 do
    begin
      if Current.Has[SupportedLangs[i]] then
        s := Current.Strings[SupportedLangs[i]]
      else
        s := '';
      frm.vleValues.InsertRow(SupportedLangs[i], s, True);
    end;
    if frm.ShowModal = mrOK then
    begin
      Result := JSON(Current.AsJSON);
      for i := 1 to frm.vleValues.RowCount-1 do
      begin
        if SupportedLangs.IndexOf(frm.vleValues.Keys[i]) < 0 then
          SupportedLangs.Add(frm.vleValues.Keys[i]);

        Result.Strings[frm.vleValues.Keys[i]] := frm.vleValues.Values[frm.vleValues.Keys[i]];
      end;
    end else
      Result := Current;
  finally
    frm.Free;
  end;
end;

procedure TfrmValues.ConfigureForStrings(DefaultStrings : TStrings);
var
  i: Integer;
begin
  FDefaultStrings.Duplicates := dupIgnore;
  FDefaultStrings.Assign(DefaultStrings);
  FDefaultStrings.Sort;

  vleValues.TitleCaptions[0] := 'ID';
  for i := 0 to FDefaultStrings.Count-1 do
  begin
    vleValues.InsertRow(FDefaultStrings.Names[i], FDefaultStrings.ValueFromIndex[i],True);
  end;

end;

class procedure TfrmValues.DefaultStringsEdit(DefaultStrings: TStrings);
var
  frm : TfrmValues;
begin
  frm := TfrmValues.Create(nil);
  try
    frm.Caption := 'Default Strings';
    frm.ConfigureForStrings(DefaultStrings);
    if frm.ShowModal = mrOK then
    begin
      frm.UpdateDefaultStrings(DefaultStrings);
    end;
  finally
    frm.Free;
  end;
end;

procedure TfrmValues.FormCreate(Sender: TObject);
begin
  FDefaultStrings := TStringList.Create;
end;

procedure TfrmValues.FormDestroy(Sender: TObject);
begin
  FDefaultStrings.Free;
end;

procedure TfrmValues.UpdateDefaultStrings(Strings : TStrings);
var
  i : integer;
begin
  for i := 1 to vleValues.RowCount-1 do
  begin
    FDefaultStrings.Values[vleValues.Keys[i]] := vleValues.Values[vleValues.Keys[i]];
  end;
  FDefaultStrings.Sort;
  Strings.Assign(FDefaultStrings);
end;

class function TfrmValues.ValueEdit(const ID: string; SupportedLangs: TStrings;
  Current: IJSONObject): IJSONObject;
var
  frm : TfrmValues;
  i: Integer;
  s : string;
begin
  frm := TfrmValues.Create(nil);
  try
    frm.Caption := 'String ID "'+ID+'"';
    for i := 0 to SupportedLangs.Count-1 do
    begin
      if Current.Has[SupportedLangs[i]] then
        s := Current.Strings[SupportedLangs[i]]
      else
        s := '';
      frm.vleValues.InsertRow(SupportedLangs[i], s, True);
    end;
    if frm.ShowModal = mrOK then
    begin
      Result := JSON(Current.AsJSON);
      for i := 1 to frm.vleValues.RowCount-1 do
      begin
        if SupportedLangs.IndexOf(frm.vleValues.Keys[i]) < 0 then
          SupportedLangs.Add(frm.vleValues.Keys[i]);

        Result.Strings[frm.vleValues.Keys[i]] := frm.vleValues.Values[frm.vleValues.Keys[i]];
      end;
    end else
      Result := Current;
  finally
    frm.Free;
  end;
end;

end.
