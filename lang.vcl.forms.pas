unit lang.vcl.forms;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, lang.manager, lang.vcl.design.editor;

type
  TLanguageForm = class(TForm)
  private
    FLang : TLangManager;
    function GetDefaultStrings: TStrings;
    procedure SetDefaultStrings(const Value: TStrings);
  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure Loaded; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Lang : TLangManager Read FLang;
  published
    property DefaultStrings : TStrings read GetDefaultStrings write SetDefaultStrings;
  end;

implementation

{ TLanguageForm }

constructor TLanguageForm.Create(AOwner: TComponent);
begin
  FLang := TLangManager.Create(Self, 'Lang');
  FLang.SetSubComponent(False);
  inherited;
  KeyPreview := True;
end;

destructor TLanguageForm.Destroy;
begin
  FLang.Free;
  inherited;
end;

function TLanguageForm.GetDefaultStrings: TStrings;
begin
  Result := FLang.DefaultStrings;
end;

procedure TLanguageForm.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = vk_Insert) and (Shift = [ssCtrl, ssAlt, ssShift]) then
    FLang.Edit;
end;

procedure TLanguageForm.Loaded;
begin
  inherited;
  FLang.Refresh;
  KeyPreview := True;
end;

procedure TLanguageForm.SetDefaultStrings(const Value: TStrings);
begin
  FLang.DefaultStrings.Assign(Value);
end;

initialization
  RegisterClass(TLanguageForm);

end.
