unit lang.fmx.design.editor;

interface

{$I lang.ver.inc}

uses
  System.SysUtils,
  System.Types,
  System.UITypes,
  System.Classes,
  System.Variants,
  FMX.Types,
  FMX.Controls,
  FMX.Forms,
  FMX.Graphics,
  FMX.Dialogs,
  FMX.Controls.Presentation,
  FMX.StdCtrls,
  FMX.Layouts,
  FMX.TreeView,
  FMX.ListBox,
  FMX.Edit,
  FMX.ComboEdit,
  lang.manager,
  System.Generics.Collections;

type
  TLangTreeItem = class(TTreeViewItem)
  private
    FCmp: TComponent;
  public
    property Cmp : TComponent read FCmp write FCmp;
  end;

  TfrmLangEditor = class(TForm)
    cbDesign: TComboEdit;
    cbSelected: TComboEdit;
    tvEditor: TTreeView;
    btnAddString: TButton;
    btnOK: TButton;
    btnCancel: TButton;
    Label1: TLabel;
    Label2: TLabel;
    procedure cbSelectedExit(Sender: TObject);
    procedure cbDesignExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure tvEditorDblClick(Sender: TObject);
    procedure btnAddStringClick(Sender: TObject);
  private
    type
      TLangEditorLauncher = class(TInterfacedObject, ILangEditor)
      public
        function Edit(Lang: TLangManager): Boolean;
      end;
    class var FLangEditorLauncher : TLangEditorLauncher;
  private
    FLang: TLangManager;
    FComponentNodes : TDictionary<TComponent, TLangTreeItem>;

    procedure SetLang(const Value: TLangManager);
    class procedure RegisterEditor;
  public
    class function Edit(Lang : TLangManager) : TModalResult;
    property Lang : TLangManager read FLang write SetLang;
  end;

implementation

uses
  System.RTTI,
  System.TypInfo,
  FMX.Menus,
  lang.fmx.design.values,
  chimera.json
  {$IFDEF TOKYOPLUS}, FMX.DialogService{$ENDIF}
  ;

{$R *.fmx}

procedure TfrmLangEditor.btnAddStringClick(Sender: TObject);
begin
  {$IFDEF TOKYOPLUS}TDialogService.{$ENDIF}InputQuery('Add String', ['New String ID'], [''],
    procedure(const AResult: TModalResult; const AValues: array of string)
    var
      i : integer;
      tnStrings, tn : TLangTreeItem;
    begin
      if AResult <> mrOK then
        exit;

      tnStrings := nil;
      for i := 0 to tvEditor.Count - 1 do
      begin
        if tvEditor.Items[i].Text = '~strings' then
        begin
          tnStrings := TLangTreeItem(tvEditor.Items[i]);
          break;
        end;
      end;
      if tnStrings = nil then
      begin
        tnStrings := TLangTreeItem.Create(Self);
        tnStrings.Text := '~strings';
        tnStrings.Parent := tvEditor;
      end;
      tn := TLangTreeItem.Create(Self);
      tn.Text := AValues[0]+' = ""';
      tn.Parent := tnStrings;
      FLang.StringValues[AValues[0]] := TfrmValues.ValueEdit(AValues[0], FLang.SupportedLangs,JSON());
      //tn.Text := sID+' = '+FLang.Strings[sID];
    end
  );
end;

procedure TfrmLangEditor.cbDesignExit(Sender: TObject);
begin
  if cbDesign.Items.IndexOf(cbDesign.Text) < 0 then
    cbDesign.Items.Add(cbDesign.Text);
  cbSelected.Items.Assign(cbDesign.Items);
end;

procedure TfrmLangEditor.cbSelectedExit(Sender: TObject);
begin
  if cbSelected.Items.IndexOf(cbSelected.Text) < 0 then
    cbSelected.Items.Add(cbSelected.Text);
  cbDesign.Items.Assign(cbSelected.Items);
end;

class function TfrmLangEditor.Edit(Lang: TLangManager): TModalResult;
var
  frm : TfrmLangEditor;
begin
  frm := TfrmLangEditor.Create(nil);
  try
    frm.Lang := Lang;
    frm.cbDesign.Items.Assign(Lang.SupportedLangs);
    frm.cbSelected.Items.Assign(Lang.SupportedLangs);
    Result := frm.ShowModal;
    if Result = mrOK then
    begin
      frm.Lang.SupportedLangs.Assign(frm.cbDesign.Items);
      frm.Lang.DesignLang := frm.cbDesign.Text;
      frm.Lang.SelectedLang:= frm.cbSelected.Text;
      frm.Lang.SaveFile;
    end;
  finally
    frm.Free;
  end;
end;

procedure TfrmLangEditor.FormCreate(Sender: TObject);
begin
  FComponentNodes := TDictionary<TComponent, TLangTreeItem>.Create;
end;

procedure TfrmLangEditor.FormDestroy(Sender: TObject);
begin
  FComponentNodes.Free;
end;

class procedure TfrmLangEditor.RegisterEditor;
begin
  //if csDesigning in ComponentState then
  //  exit;
  FLangEditorLauncher := TLangEditorLauncher.Create;
  TLangManager.Editor := FLangEditorLauncher;
end;

procedure TfrmLangEditor.SetLang(const Value: TLangManager);
  procedure IterateComponents(cmp : TComponent; ParentNode : TLangTreeItem);
    procedure IterateProperties(cmp : TComponent; ParentNode : TLangTreeItem);
    var
      cls : TObject;
      cxt : TRTTIContext;
      t, t2 : TRTTIType;
      p, p2 : TRTTIProperty;
      tn : TLangTreeItem;
    begin
      if cmp.Name = '' then
        exit;
      t := cxt.GetType(cmp.ClassType);
      for p in t.GetProperties do
      begin
        if p.PropertyType.TypeKind in [
            tkString, tkWideString, tkChar, tkWideChar,
            tkLString, tkWString, tkUString, tkAnsiChar,
            tkUnicodeString, tkAnsiString, tkShortString] then
        begin
          if (p.Name = 'Name') or
             (p.Name.Trim = '') or
             (p.Name = 'StyleLookup') or
             (p.Name = 'StyleName') or
             (P.Visibility <> TMemberVisibility.mvPublished) or
             (not p.IsReadable) or
             (not p.IsWritable) then
            Continue;
          if FLang.SelectedLang = FLang.DesignLang then
            FLang.Properties[cmp,p.Name] := p.GetValue(cmp).AsString;
          tn := TLangTreeItem.Create(Self);
          tn.Text := p.Name+' = "'+FLang.Properties[cmp,p.Name];
          tn.Parent := ParentNode;
        end else if p.PropertyType.TypeKind in [tkClassRef, tkClass] then
        begin
          if (p.Name = 'Model') or
             (p.Name.Trim = '') or
             (p.Name = 'Parent') or
             (p.Name = 'Owner') or
             (p.Name = 'Font') or
             (p.Name = 'Presentation') or
             (p.Name = 'PlacementTarget') or
             (p.Name = 'ButtonsContent') or
             (p.Name.Trim = '') or
             (not (P.Visibility in [TMemberVisibility.mvPublic, TMemberVisibility.mvPublished])) then
            Continue;
          cls := p.GetValue(cmp).AsObject;
          if Assigned(cls) then
          begin
            t2 := cxt.GetType(cls.ClassType);
            for p2 in t2.GetProperties do
            begin
              if p2.PropertyType.TypeKind in [
                  tkString, tkWideString, tkChar, tkWideChar,
                  tkLString, tkWString, tkUString, tkAnsiChar,
                  tkUnicodeString, tkAnsiString, tkShortString] then
              begin
                if (p2.Name = 'Name') or
                   (p2.Name.Trim = '') or
                   (p2.Name = 'StyleLookup') or
                   (p2.Name = 'StyleName') or
                   (p2.Name = 'LineBreak') or
                   (p2.Name = 'Delimiter') or
                   (p2.Name = 'QuoteChar') or
                   (not (P2.Visibility in [TMemberVisibility.mvPublic, TMemberVisibility.mvPublished])) or
                   (not p2.IsReadable) or
                   (not p2.IsWritable) then
                  Continue;
                if FLang.SelectedLang = FLang.DesignLang then
                  FLang.Properties[cmp,p.Name+'.'+p2.Name] := p2.GetValue(cls).AsString;
                tn := TLangTreeItem.Create(Self);
                tn.Text := p.Name+'.'+p2.Name+' = "'+FLang.Properties[cmp,p.Name+'.'+p2.Name];
                tn.Parent := ParentNode;
              end;
            end;
          end;
        end;
      end;
    end;
  var
    i : integer;
    tn : TLangTreeItem;
  begin
    for i := 0 to cmp.ComponentCount-1 do
    begin
      if cmp.Components[i] <> FLang then
      begin
        tn := TLangTreeItem.Create(Self);
        tn.Text := cmp.Components[i].Name + ' : '+cmp.Components[i].ClassName;
        tn.Cmp := cmp.Components[i];
        if ParentNode = nil then
          tn.Parent := tvEditor
        else
          tn.Parent := ParentNode;
        FComponentNodes.Add( cmp.Components[i], tn);
        IterateProperties(cmp.Components[i], tn);
        IterateComponents(cmp.Components[i], tn);
      end;
    end;
  end;

  procedure ReparentNodes;
    procedure ChangeNodeParent(tn : TLangTreeItem; NewParent : TLangTreeItem; ListToFree : TList<TLangTreeItem>);
    var
      tnNew, tnItem : TLangTreeItem;
      i: Integer;
      cmp : TComponent;
    begin
      cmp := TComponent(tn.Cmp);
      tnNew := TLangTreeItem.Create(Self);
      tnNew.Text := cmp.Name+' : '+cmp.ClassName;
      tnNew.Cmp := cmp;
      tnNew.Parent := NewParent;
      FComponentNodes.AddOrSetValue(cmp, tnNew);
      for i := tn.Count-1 downto 0 do
      begin
        if TLangTreeItem(tn.Items[i]).Cmp <> nil then
        begin
          ChangeNodeParent(TLangTreeItem(tn.Items[i]), tnNew, ListToFree);
        end else
        begin
          tnItem := TLangTreeItem.Create(Self);
          tnItem.Text := tn.Items[i].Text;
          tnItem.Parent := tnNew;
        end;
      end;
      ListToFree.Add(tn);
    end;
  var
    cmp : TComponent;
    i, iCnt : integer;
    lti : TLangTreeItem;
    lstFree : TList<TLangTreeItem>;
  begin
    lstFree := TList<TLangTreeItem>.Create;
    try
      iCnt := tvEditor.Count;
      for i := 0 to iCnt - 1 do
      begin
        lti := TLangTreeItem(tvEditor.Items[i]);
        if lti.Cmp = nil then
          Continue;
        cmp := TComponent(lti.Cmp);
        if cmp is TControl then
        begin
          if TControl(cmp).Parent <> FLang.Root then
          begin
            ChangeNodeParent(lti, FComponentNodes[TControl(cmp).Parent], lstFree);
          end;
        end else if cmp is TMenuItem then
        begin
          if TMenuItem(cmp).Parent <> nil then
          begin
            ChangeNodeParent(lti, FComponentNodes[TMenuItem(cmp).Parent], lstFree);
          end;
        end;
      end;
      while lstFree.Count > 0 do
      begin
        lti := lstFree.First;
        lstFree.Remove(lti);
        lti.Free;
      end;
    finally
      lstFree.Free;
    end;
  end;

var
  tnStrings : TLangTreeItem;
begin
  FLang := Value;
  cbDesign.Items.Assign(FLang.SupportedLangs);
  cbSelected.Items.Assign(FLang.SupportedLangs);
  cbDesign.Text := FLang.DesignLang;
  cbSelected.Text := FLang.SelectedLang;
  FComponentNodes.Clear;
  tvEditor.Clear;

  IterateComponents(FLang.Root, nil);
  ReparentNodes;
  tnStrings := TLangTreeItem.Create(Self);
  tnStrings.Text := '~strings';
  tnStrings.Parent := tvEditor;
  FLang.EachID(
    procedure(const ID : string; const Obj : IJSONObject)
    var
      tn : TLangTreeItem;
      s : string;
    begin
      if Obj.Has[FLang.SelectedLang] then
        s := Obj.Strings[FLang.SelectedLang]
      else
        s := '';
      tn := TLangTreeItem.Create(Self);
      tn.Text := ID+' = '+s;
      tn.Parent := tnStrings;
    end
  );
end;

procedure TfrmLangEditor.tvEditorDblClick(Sender: TObject);
var
  sProp : string;
begin
  if (tvEditor.Selected <> nil) and (tvEditor.Selected.ParentItem <> nil) and (tvEditor.Selected.ParentItem.Text = '~strings') then
  begin
    sProp := tvEditor.Selected.Text.Split(['='])[0].Trim;
    FLang.StringValues[sProp] := TfrmValues.ValueEdit(sProp, FLang.SupportedLangs, FLang.StringValues[sProp]);
  end else if (tvEditor.Selected <> nil) and (TLangTreeItem(tvEditor.Selected).Cmp = nil) and (tvEditor.Selected.ParentItem <> nil) then
  begin
    sProp := tvEditor.Selected.Text.Split(['='])[0].Trim;
    FLang.Values[TComponent(TLangTreeItem(tvEditor.Selected.ParentItem).Cmp), sProp] := TfrmValues.ValueEdit(TComponent(TLangTreeItem(tvEditor.Selected.ParentItem).Cmp), sProp, FLang.SupportedLangs, FLang.Values[TComponent(TLangTreeItem(tvEditor.Selected.ParentItem).Cmp), sProp]);
  end;
  cbSelected.Items.Assign(FLang.SupportedLangs);
  cbDesign.Items.Assign(FLang.SupportedLangs);
end;

{ TfrmLangEditor.TLangEditorLauncher }

function TfrmLangEditor.TLangEditorLauncher.Edit(Lang: TLangManager): Boolean;
begin
  Result := TfrmLangEditor.Edit(Lang) = mrOK;
end;

initialization
  TfrmLangEditor.RegisterEditor;

end.
