unit lang.datamodule;

interface

uses
  System.SysUtils, System.Classes, lang.manager;

type
  TLanguageModule = class(TDataModule)
  private
    FLang : TLangManager;
    function GetDefaultStrings: TStrings;
    procedure SetDefaultStrings(const Value: TStrings);
    procedure Loaded; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Lang : TLangManager Read FLang;
  published
    property DefaultStrings : TStrings read GetDefaultStrings write SetDefaultStrings;
  end;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{ TLanguageModule }

constructor TLanguageModule.Create(AOwner: TComponent);
begin
  inherited;
  FLang := TLangManager.Create(nil);
  FLang.Name := 'Lang';
  FLang.SetSubComponent(False);
end;

destructor TLanguageModule.Destroy;
begin
  FLang.Free;
  inherited;
end;

function TLanguageModule.GetDefaultStrings: TStrings;
begin
  Result := FLang.DefaultStrings;
end;

procedure TLanguageModule.Loaded;
begin
  inherited;

end;

procedure TLanguageModule.SetDefaultStrings(const Value: TStrings);
begin
  FLang.DefaultStrings.Assign(Value);
end;

initialization
  RegisterClass(TLanguageModule);

end.
