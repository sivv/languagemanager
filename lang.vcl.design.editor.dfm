object frmLangEditor: TfrmLangEditor
  Left = 0
  Top = 0
  BorderIcons = [biMaximize]
  Caption = 'Language Editor'
  ClientHeight = 413
  ClientWidth = 578
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    578
    413)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 82
    Height = 13
    Caption = 'Design Language'
  end
  object Label2: TLabel
    Left = 424
    Top = 8
    Width = 91
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Selected Language'
    ExplicitLeft = 168
  end
  object cbDesign: TComboBox
    Left = 8
    Top = 27
    Width = 145
    Height = 21
    TabOrder = 0
    OnExit = cbDesignExit
  end
  object cbSelected: TComboBox
    Left = 424
    Top = 27
    Width = 145
    Height = 21
    Anchors = [akTop, akRight]
    TabOrder = 1
    OnExit = cbSelectedExit
  end
  object tvEditor: TTreeView
    Left = 8
    Top = 54
    Width = 561
    Height = 306
    Anchors = [akLeft, akTop, akRight, akBottom]
    Indent = 19
    ReadOnly = True
    TabOrder = 2
    OnDblClick = tvEditorDblClick
  end
  object btnOK: TButton
    Left = 8
    Top = 377
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 3
  end
  object btnCancel: TButton
    Left = 494
    Top = 377
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
  object btnAddString: TButton
    Left = 252
    Top = 23
    Width = 75
    Height = 25
    Anchors = [akTop]
    Caption = 'Add String'
    TabOrder = 5
    OnClick = btnAddStringClick
  end
end
