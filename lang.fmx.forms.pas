unit lang.fmx.forms;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, lang.manager,
  lang.fmx.design.editor;

type
  TLanguageForm = class(TForm)
  private
    FLang : TLangManager;
    function GetDefaultStrings: TStrings;
    procedure SetDefaultStrings(const Value: TStrings);
  protected
    procedure Loaded; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure KeyUp(var Key: Word; var KeyChar: Char; Shift: TShiftState);
      override;

    property Lang : TLangManager Read FLang;
  published
    property DefaultStrings : TStrings read GetDefaultStrings write SetDefaultStrings;
  end;

implementation

{ TLanguageForm }

constructor TLanguageForm.Create(AOwner: TComponent);
begin
  FLang := TLangManager.Create(Self, 'Lang');
  FLang.SetSubComponent(False);
  inherited;
end;

destructor TLanguageForm.Destroy;
begin
  FLang.Free;
  inherited;
end;

function TLanguageForm.GetDefaultStrings: TStrings;
begin
  Result := FLang.DefaultStrings;
end;

procedure TLanguageForm.KeyUp(var Key: Word; var KeyChar: Char;
  Shift: TShiftState);
begin
  inherited;
  if (Key = vkInsert) and (Shift = [ssCtrl, ssAlt, ssShift]) then
    FLang.Edit;
end;

procedure TLanguageForm.Loaded;
begin
  inherited;
  Lang.Refresh;
end;

procedure TLanguageForm.SetDefaultStrings(const Value: TStrings);
begin
  FLang.DefaultStrings.Assign(Value);
end;

initialization
  RegisterClass(TLanguageForm);


end.
