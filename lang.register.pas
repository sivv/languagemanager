unit lang.register;

interface

procedure Register;

implementation

uses System.Classes, DesignIntf, DesignEditors, lang.manager, lang.datamodule,
  System.SysUtils, ToolsAPI, lang.design.stringsframe, Vcl.Menus, duck, Vcl.Forms;

var
  FDocker : INTACustomDockableForm;

procedure RegisterDockForm;
var
  ide : INTAServices;
  miView: TMenuItem;
  mi : TMenuItem;
begin
  ide := (BorlandIDEServices as INTAServices);
  FDocker := TDockableLangStrings.Create(
    procedure(Sender : TObject)
    begin
      ide.UnregisterDockableForm(FDocker);
      FDocker := nil;
      RegisterDockForm;
    end
  );
  ide.RegisterDockableForm(FDocker);

  ide.MenuBeginUpdate;
  try
    miView := ide.MainMenu.Items[3];
    if miView.FindComponent('LangStringTools') <> nil then
      exit;

    mi := TMenuItem.Create(miView);
    mi.Caption := '-';
    miView.Add(mi);

    mi := TMenuItem.Create(miView);
    mi.Caption := 'Show Lang Strings';
    mi.Name := 'LangStringTools';
    mi.OnClick := Event.Notify(procedure(Sender : TObject)
      begin
        ide.CreateDockableForm(FDocker);
      end);
    miView.Add(mi);
  finally
    ide.MenuEndUpdate;
  end;
end;

var
  _KeyboardBindingIndex : integer;

procedure BindKeystrokes;
var
  kb : IOTAKeyboardBinding;
begin
  kb := TKeyboardBinding.Create;
  _KeyboardBindingIndex := (BorlandIDEServices as IOTAKeyboardServices).AddKeyboardBinding(kb);
end;

procedure UnregisterDockForm;
begin
  (BorlandIDEServices as IOTAKeyboardServices).RemoveKeyboardBinding(_KeyboardBindingIndex);
  (BorlandIDEServices as INTAServices).UnregisterDockableForm(FDocker);
  FDocker := nil;
end;

procedure Register;
begin
  RegisterComponents('Language', [TLangManager]);
  RegisterCustomModule(TLanguageModule, TCustomModule);
  BindKeystrokes;
  RegisterDockForm;
end;

initialization
finalization
  UnregisterDockForm;

end.
