object frmDefaultStrings: TfrmDefaultStrings
  Left = 0
  Top = 0
  Width = 320
  Height = 240
  TabOrder = 0
  object vleStrings: TValueListEditor
    Left = 0
    Top = 19
    Width = 320
    Height = 132
    Align = alClient
    KeyOptions = [keyEdit, keyAdd, keyDelete, keyUnique]
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goEditing, goTabs, goThumbTracking]
    TabOrder = 0
    TitleCaptions.Strings = (
      'ID'
      'Value')
    OnDblClick = vleStringsDblClick
    OnExit = vleStringsExit
    OnGetEditText = vleStringsGetEditText
    OnKeyUp = vleStringsKeyUp
    OnSelectCell = vleStringsSelectCell
    OnSetEditText = vleStringsSetEditText
    OnTopLeftChanged = ResetTimer
    ColWidths = (
      150
      164)
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 0
    Width = 320
    Height = 19
    Align = alTop
    Panels = <>
    SimplePanel = True
    SizeGrip = False
    OnClick = StatusBarClick
  end
  object cbLangs: TComboBox
    Left = 0
    Top = 19
    Width = 320
    Height = 21
    Align = alClient
    Style = csDropDownList
    DropDownCount = 20
    TabOrder = 2
    Visible = False
    OnChange = cbLangsChange
    OnExit = cbLangsExit
  end
  object txtLog: TMemo
    Left = 0
    Top = 151
    Width = 320
    Height = 89
    Align = alBottom
    Lines.Strings = (
      'txtLog')
    TabOrder = 3
    Visible = False
  end
  object tmrUpdate: TTimer
    Interval = 500
    OnTimer = tmrUpdateTimer
    Left = 144
    Top = 104
  end
end
