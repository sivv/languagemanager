unit lang.fmx.design.register;

interface

uses System.Classes, DesignIntf, DesignEditors;

procedure Register;

implementation

uses lang.fmx.forms;

procedure Register;
begin
  RegisterCustomModule(TLanguageForm, TCustomModule);
end;

end.
