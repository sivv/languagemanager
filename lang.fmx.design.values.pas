unit lang.fmx.design.values;

interface

{$I lang.ver.inc}

uses
  System.SysUtils, 
  System.Types, 
  System.UITypes, 
  System.Classes, 
  System.Variants,
  FMX.Types, 
  FMX.Controls, 
  FMX.Forms, 
  FMX.Graphics, 
  FMX.Dialogs, 
  System.Rtti,
  FMX.StdCtrls, 
  FMX.Grid, 
  FMX.Controls.Presentation,
  FMX.ScrollBox, 
  chimera.json, 
  FMX.Layouts, 
  FMX.Memo, 
  FMX.Objects,
  FMX.Grid.Style,
  FMX.Memo.Types
  ;

type
  TfrmValues = class(TForm)
    sgValues: TStringGrid;
    colLanguage: TStringColumn;
    colValue: TStringColumn;
    btnOK: TButton;
    btnCancel: TButton;
    pnlMemo: TPanel;
    txtMemo: TMemo;
    pnlMemoButtons: TPanel;
    btnCloseMemo: TButton;
    txtProp: TText;
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure txtMemoKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure btnCloseMemoClick(Sender: TObject);
    procedure sgValuesCellDblClick(const Column: TColumn; const Row: Integer);
  private
    procedure ShowMemo(Row : Integer);
    procedure CancelMemo;
    procedure PostMemo;
  public
    class function ValueEdit(cmp : TComponent; const Prop : string; SupportedLangs : TStrings; Current : IJSONObject) : IJSONObject; overload;
    class function ValueEdit(const ID : string; SupportedLangs : TStrings; Current : IJSONObject) : IJSONObject; overload;
  end;

var
  frmValues: TfrmValues;

implementation

{$R *.fmx}

procedure TfrmValues.btnCloseMemoClick(Sender: TObject);
begin
  PostMemo;
end;

procedure TfrmValues.CancelMemo;
begin
  pnlMemo.Visible := False;
end;

procedure TfrmValues.FormCreate(Sender: TObject);
begin
  sgValues.Options := [
    TGridOption.AlternatingRowBackground,
    TGridOption.Editing,
    TGridOption.AlwaysShowEditor,
    TGridOption.ColumnResize,
    TGridOption.ColumnMove,
    TGridOption.ColLines,
    TGridOption.RowLines,
    TGridOption.Tabs,
    TGridOption.Header
    {$IFDEF TOKYOPLUS}
    ,TGridOption.AutoDisplacement
    {$ENDIF}
  ];
end;

procedure TfrmValues.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
var
  NewKey : Word;
begin
  if pnlMemo.Visible then
    exit;
  if (Key = vkDown) and (sgValues.Selected = sgValues.RowCount-1) then
  begin
    sgValues.RowCount := sgValues.RowCount+1;
    sgValues.Selected := sgValues.RowCount-1;
  end else if (Key = vkUp) and (sgValues.Selected = sgValues.RowCount-1) then
  begin
    if (sgValues.Cells[0,sgValues.RowCount-1] = '') and (sgValues.Cells[1,sgValues.RowCount-1] = '') then
    begin
      sgValues.RowCount := sgValues.RowCount-1;
    end;
  end else if (Key = vkReturn) then
  begin
    if Shift = [ssCtrl] then
    begin
      ShowMemo(sgValues.Selected);
    end else
    begin
      NewKey := vkDown;
      FormKeyUp(Sender, NewKey, KeyChar, Shift);
    end;
  end;

end;

procedure TfrmValues.FormShow(Sender: TObject);
begin
  sgValues.Columns[0].Header := 'Language';
  sgValues.Columns[1].Header := 'Text';
end;

procedure TfrmValues.PostMemo;
begin
  sgValues.Cells[1, sgValues.Selected] := txtMemo.Text;
  pnlMemo.Visible := False;
end;

procedure TfrmValues.sgValuesCellDblClick(const Column: TColumn;
  const Row: Integer);
begin
  ShowMemo(Row);
end;

procedure TfrmValues.ShowMemo(Row : Integer);
begin
  if Row < sgValues.RowCount then
  begin
    txtMemo.Text := sgValues.Cells[1, Row];
    pnlMemo.Visible := True;
  end;
end;

procedure TfrmValues.txtMemoKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if Key = vkEscape then
    CancelMemo
  else if (Key = vkReturn) and (Shift = [ssCtrl]) then
    PostMemo;

end;

class function TfrmValues.ValueEdit(const ID: string; SupportedLangs: TStrings;
  Current: IJSONObject): IJSONObject;
var
  frm : TfrmValues;
  i: Integer;
  s : string;
begin
  frm := TfrmValues.Create(nil);
  try
    frm.Caption := 'String ID "'+ID+'"';
    frm.sgValues.RowCount := SupportedLangs.Count;
    frm.sgValues.BeginUpdate;
    try
      for i := 0 to SupportedLangs.Count-1 do
      begin
        if Current.Has[SupportedLangs[i]] then
          s := Current.Strings[SupportedLangs[i]]
        else
          s := '';
        frm.sgValues.Cells[0, i] := SupportedLangs[i];
        frm.sgValues.Cells[1, i] := s;
      end;
    finally
      frm.sgValues.EndUpdate;
    end;
    if frm.ShowModal = mrOK then
    begin
      Result := JSON(Current.AsJSON);
      for i := 0 to frm.sgValues.RowCount-1 do
      begin
        if SupportedLangs.IndexOf(frm.sgValues.Cells[0,i]) < 0 then
          SupportedLangs.Add(frm.sgValues.Cells[0, i]);

        Result.Strings[frm.sgValues.Cells[0, i]] := frm.sgValues.Cells[1,i];
      end;
    end else
      Result := Current;
  finally
    frm.Free;
  end;
end;

class function TfrmValues.ValueEdit(cmp: TComponent; const Prop: string;
  SupportedLangs: TStrings; Current: IJSONObject): IJSONObject;
var
  frm : TfrmValues;
  i: Integer;
  s : string;
begin
  frm := TfrmValues.Create(nil);
  try
    frm.Caption := 'Values for '+cmp.Name+'.'+Prop;
    frm.sgValues.RowCount := SupportedLangs.Count;
    frm.sgValues.BeginUpdate;
    try
      for i := 0 to SupportedLangs.Count-1 do
      begin
        if Current.Has[SupportedLangs[i]] then
          s := Current.Strings[SupportedLangs[i]]
        else
          s := '';
        frm.sgValues.Cells[0, i] := SupportedLangs[i];
        frm.sgValues.Cells[1, i] := s;
      end;
    finally
      frm.sgValues.EndUpdate;
    end;
    if frm.ShowModal = mrOK then
    begin
      Result := JSON(Current.AsJSON);
      for i := 0 to frm.sgValues.RowCount-1 do
      begin
        if SupportedLangs.IndexOf(frm.sgValues.Cells[0, i]) < 0 then
          SupportedLangs.Add(frm.sgValues.Cells[0, i]);

        Result.Strings[frm.sgValues.Cells[0, i]] := frm.sgValues.Cells[1, i];
      end;
    end else
      Result := Current;
  finally
    frm.Free;
  end;
end;

end.
