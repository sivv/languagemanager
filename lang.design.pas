unit lang.design;

interface

uses System.Classes, System.SysUtils, DesignIntf, DesignEditors, ToolsAPI,
  lang.manager;

type
  TLangEditor = class(TComponentEditor)
  public
    function GetVerbCount: Integer; override;
    function GetVerb(Index: Integer): string; override;
    procedure ExecuteVerb(Index: Integer); override;
  end;

  TDefaultStringsEditor = class(TPropertyEditor)
  private
  public
    procedure Edit; override;
    function GetAttributes: TPropertyAttributes; override;
    function GetValue: string; override;
  end;

procedure Register;


implementation

uses lang.vcl.design.editor, lang.vcl.design.values;

procedure Register;
begin
  RegisterComponentEditor(TLangManager, TLangEditor);
  RegisterPropertyEditor(TypeInfo(TDefaultStrings),nil, '',TDefaultStringsEditor);
end;


{ TLangEditor }

procedure TLangEditor.ExecuteVerb(Index: Integer);
begin
  inherited;
  //TfrmLangEditor.Edit(TLangManager(Component));
  TfrmValues.DefaultStringsEdit(TLangManager(Component).DefaultStrings);
end;

function TLangEditor.GetVerb(Index: Integer): string;
begin
  Result := 'Default Strings';
end;

function TLangEditor.GetVerbCount: Integer;
begin
  Result := 1;
end;

{ TDefaultStringsEditor }

procedure TDefaultStringsEditor.Edit;
begin
  TfrmValues.DefaultStringsEdit(TDefaultStrings(GetOrdValue));
end;

function TDefaultStringsEditor.GetAttributes: TPropertyAttributes;
begin
  Result := [TPropertyAttribute.paDialog];
end;

function TDefaultStringsEditor.GetValue: string;
begin
  Result := TDefaultStrings(GetOrdValue).Count.ToString+' Strings';
end;

end.
