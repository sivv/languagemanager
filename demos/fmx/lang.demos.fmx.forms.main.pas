unit lang.demos.fmx.forms.main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Menus,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit, lang.manager, lang.fmx.forms;

type
  TfrmMain = class(TLanguageForm)
    MainMenu: TMainMenu;
    miFile: TMenuItem;
    miExit: TMenuItem;
    miLanguage: TMenuItem;
    miEnglish: TMenuItem;
    miSpanish: TMenuItem;
    miKlingon: TMenuItem;
    miN1: TMenuItem;
    miEdit: TMenuItem;
    txtUsername: TEdit;
    txtPassword: TEdit;
    lblUsername: TLabel;
    lblPassword: TLabel;
    btnLogin: TButton;
    procedure btnLoginClick(Sender: TObject);
    procedure Edit1Click(Sender: TObject);
    procedure ChangeLang(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses lang.fmx.design.editor;

{$R *.fmx}

procedure TfrmMain.btnLoginClick(Sender: TObject);
begin
  if (txtUsername.Text = 'User') and (txtPassword.Text = 'Password') then
    ShowMessage(Lang.Strings['Login_Successful'])
  else
    ShowMessage(Lang.Strings['Login_Failed']);
end;

procedure TfrmMain.ChangeLang(Sender: TObject);
begin
  Lang.SelectedLang := TMenuItem(Sender).TagString;
end;

procedure TfrmMain.Edit1Click(Sender: TObject);
begin
  Lang.Edit;
end;

procedure TfrmMain.Exit1Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  miEnglish.TagString := 'English';
  miSpanish.TagString := 'Spanish';
  miKlingon.TagString := 'Klingon';
end;

end.
