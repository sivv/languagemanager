program lang.demos.fmx;

uses
  System.StartUpCopy,
  FMX.Forms,
  lang.demos.fmx.forms.main in 'lang.demos.fmx.forms.main.pas' {frmMain};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
