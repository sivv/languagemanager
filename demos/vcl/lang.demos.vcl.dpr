program lang.demos.vcl;

uses
  Vcl.Forms,
  lang.demos.vcl.forms.main in 'lang.demos.vcl.forms.main.pas' {frmMain};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
