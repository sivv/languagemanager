object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'VCL Demo'
  ClientHeight = 174
  ClientWidth = 261
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 40
    Top = 13
    Width = 48
    Height = 13
    Caption = 'Username'
  end
  object Label2: TLabel
    Left = 40
    Top = 61
    Width = 46
    Height = 13
    Caption = 'Password'
  end
  object txtUsername: TEdit
    Left = 40
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object Button1: TButton
    Left = 40
    Top = 115
    Width = 75
    Height = 25
    Caption = 'Login'
    TabOrder = 1
    OnClick = Button1Click
  end
  object txtPassword: TEdit
    Left = 40
    Top = 80
    Width = 121
    Height = 21
    PasswordChar = '*'
    TabOrder = 2
  end
  object MainMenu1: TMainMenu
    Left = 184
    Top = 8
    object File1: TMenuItem
      Caption = '&File'
      object Exit1: TMenuItem
        Caption = 'E&xit'
        OnClick = Exit1Click
      end
    end
    object Language1: TMenuItem
      Caption = '&Language'
      object English1: TMenuItem
        Caption = 'English'
        Hint = 'English'
        OnClick = ChangeLang
      end
      object Spanish1: TMenuItem
        Caption = 'Spanish'
        Hint = 'Spanish'
        OnClick = ChangeLang
      end
      object Klingon1: TMenuItem
        Caption = 'Klingon'
        Hint = 'Klingon'
        OnClick = ChangeLang
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Edit1: TMenuItem
        Caption = 'Edit'
        OnClick = Edit1Click
      end
    end
  end
end
