unit lang.demos.vcl.forms.main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Menus, lang.manager, lang.vcl.forms;

type
  TfrmMain = class(TLanguageForm)
    txtUsername: TEdit;
    Label1: TLabel;
    Button1: TButton;
    txtPassword: TEdit;
    Label2: TLabel;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Language1: TMenuItem;
    English1: TMenuItem;
    Spanish1: TMenuItem;
    Klingon1: TMenuItem;
    Exit1: TMenuItem;
    N1: TMenuItem;
    Edit1: TMenuItem;
    procedure Button1Click(Sender: TObject);
    procedure Edit1Click(Sender: TObject);
    procedure ChangeLang(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
  private
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses lang.vcl.design.editor;

{$R *.dfm}

procedure TfrmMain.Button1Click(Sender: TObject);
begin
  if (txtUsername.Text = 'User') and (txtPassword.Text = 'Password') then
    ShowMessage(Lang.Strings['Login_Successful'])
  else
    ShowMessage(Lang.Strings['Login_Failed']);
end;

procedure TfrmMain.ChangeLang(Sender: TObject);
begin
  Lang.SelectedLang := TMenuItem(Sender).Hint;
end;

procedure TfrmMain.Edit1Click(Sender: TObject);
begin
  Lang.Edit;
end;

procedure TfrmMain.Exit1Click(Sender: TObject);
begin
  Application.Terminate;
end;

end.
