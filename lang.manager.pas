unit lang.manager;

interface

uses System.SysUtils, System.Classes, System.Generics.Collections, chimera.json;

type
  TLangManager = class;

  TDefaultStrings = class(TStringList)

  end;

  ILangEditor = interface
    function Edit(Lang : TLangManager) : boolean;
  end;

  TLangManager = class(TComponent)
  private
    class var FLangs : TList<TLangManager>;
    class var FDataFolder: string;
    class var FEditor: ILangEditor;
    class var FDesignLang : string;
    class var FSelectedLang : string;
    class procedure SetSelectedLang(const Value: string); static;
    class procedure SetDesignLang(const Value: string); static;
  private
    FLoaded : boolean;
    FJSON : IJSONObject;
    FUpdatedLang : boolean;
    FSettingsJSON : IJSONObject;
    FJSONFilename: string;
    FSupportedLangs: TStrings;
    FDefaultStrings: TDefaultStrings;
    FRoot: TComponent;
    function GetString(const id: string): string;
    procedure SetString(const id: string; const Value: string);
    function GetProperty(cmp: TComponent; const prop: string): string;
    procedure SetProperty(cmp: TComponent; const prop: string; const Value: string);
    procedure SetSupportedLangs(const Value: TStrings);
    procedure UpdateRootProperties;
    function GetValues(cmp: TComponent; const prop: string): IJSONObject;
    procedure SetValues(cmp: TComponent; const prop: string;
      const Value: IJSONObject);
    function GetStringValues(const id: string): IJSONObject;
    procedure SetStringValues(const id: string; const Value: IJSONObject);
    procedure EnsureLoaded;
    procedure SetDefaultStrings(const Value: TDefaultStrings);
    function ApplicationName : string;
    procedure SetRoot(const Value: TComponent);
  protected
    procedure Loaded; override;
  public
    constructor Create(ARoot : TComponent; const AName : string); overload;
    constructor Create(AOwner: TComponent); overload; override;
    destructor Destroy; override;

    class constructor Create;
    class destructor Destroy;

    procedure Edit;
    procedure SaveFile;
    procedure Refresh;

    property Root : TComponent read FRoot write SetRoot;

    property Strings[const id : string] : string read GetString write SetString; default;
    property Properties[cmp : TComponent; const prop : string] : string read GetProperty write SetProperty;
    property Values[cmp : TComponent; const prop : string] : IJSONObject read GetValues write SetValues;
    property StringValues[const id : string] : IJSONObject read GetStringValues write SetStringValues;
    procedure EachID(proc : TProcConst<string, IJSONObject>);

    class property DataFolder : string read FDataFolder write FDataFolder;
    class property Editor : ILangEditor read FEditor write FEditor;
    class property SelectedLang : string read FSelectedLang write SetSelectedLang;
    class property DesignLang : string read FDesignLang write SetDesignLang;
  published
    property SupportedLangs : TStrings read FSupportedLangs write SetSupportedLangs;
    property DefaultStrings : TDefaultStrings read FDefaultStrings write SetDefaultStrings;
  end;

implementation

uses System.IOUtils, System.RTTI;

{ TLangManager }

procedure TLangManager.UpdateRootProperties;
begin
  if FSupportedLangs.IndexOf(SelectedLang) < 0 then
    FSupportedLangs.Add(SelectedLang);
  FSettingsJSON.Strings['selected'] := SelectedLang;
  if FSupportedLangs.IndexOf(DesignLang) < 0 then
    FSupportedLangs.Add(DesignLang);
  FSettingsJSON.Strings['design'] := DesignLang;

  if FJSON = nil then
    Exit;
  FJSON.Each(
    procedure(const Name : string; const Obj : IJSONObject)
    var
      cmp : TComponent;
    begin
      if (Name = '~strings') then
        exit;
      if Root = nil then
        exit;
      cmp := Root.FindComponent(Name);
      if Assigned(cmp) then
      begin
        Obj.Each(
          procedure(const Prop : string; const Langs : IJSONObject)
          var
            cxt : TRTTIContext;
            pt : TRTTIType;
            pp : TRTTIProperty;
            props : TArray<String>;
            cls : TObject;
          begin
            pt := cxt.GetType(cmp.ClassType);
            if Prop.Contains('.') then
            begin
              props := Prop.Split(['.']);
              pp := pt.GetProperty(props[0]);
              if Assigned(pp) then
              begin
                cls := pp.GetValue(cmp).AsObject;
                if Assigned(cls) then
                begin
                  pt := cxt.GetType(cls.ClassType);
                  pp := pt.GetProperty(props[1]);
                  if Assigned(pp) and pp.IsWritable then
                  begin
                    if Langs.Has[SelectedLang] then
                      try
                        pp.SetValue(cls, TValue.From<string>(Langs.Strings[SelectedLang]));
                      except
                        // eat exceptions
                      end;
                  end;
                end;
              end;
            end else
            begin
              pp := pt.GetProperty(Prop);
              if Assigned(pp) then
              begin
                if Langs.Has[SelectedLang] then
                  pp.SetValue(cmp,TValue.From<string>(Langs.Strings[SelectedLang]));
              end;
            end;
          end
        );
      end;
    end
  );
end;

constructor TLangManager.Create(AOwner: TComponent);
//var
  {ModuleServices: IOTAModuleServices;
  Module: IOTAModule;
  SourceEditor: IOTASourceEditor;}
//  idx: integer;
begin
  inherited;
  FRoot := AOwner;
  FDefaultStrings := TDefaultStrings.Create;
  TMonitor.Enter(FLangs);
  try
    FLangs.Add(Self);
  finally
    TMonitor.Exit(FLangs);
  end;

  if FDataFolder = '' then
    FDataFolder := TPath.Combine(TPath.GetLibraryPath, 'lang_data');;

  if not TDirectory.Exists(FDataFolder) then
    TDirectory.CreateDirectory(FDataFolder);
  FSettingsJSON := JSON;
  FSupportedLangs := TStringList.Create;
  TStringList(FSupportedLangs).Sorted := True;
  TStringList(FSupportedLangs).Duplicates := dupIgnore;
  FSupportedLangs.Add(SelectedLang);
  FSupportedLangs.Add(DesignLang);
end;

destructor TLangManager.Destroy;
begin
  TMonitor.Enter(FLangs);
  try
    FLangs.Remove(Self);
  finally
    TMonitor.Exit(FLangs);
  end;
  FSupportedLangs.Free;
  FJSON := nil;
  FDefaultStrings.Free;
  inherited;
end;

class destructor TLangManager.Destroy;
begin
  FLangs.Free;
end;

procedure TLangManager.EachID(proc: TProcConst<string, IJSONObject>);
var
  slFound : TStringList;
  i: Integer;
  jso : IJSONObject;
  j: Integer;
begin
  if FJSON.Has['~strings'] then
  begin
    slFound := TStringList.Create;
    try
      for i := 0 to FDefaultStrings.Count-1 do
      begin
        jso := JSON;

        for j := 0 to FSupportedLangs.Count-1 do
          if FJSON.Objects['~strings'].Has[FDefaultStrings.Names[i]] and
             FJSON.Objects['~strings'].Objects[FDefaultStrings.Names[i]].Has[FSupportedLangs[j]] then
            jso.Strings[FSupportedLangs[j]] := FJSON.Objects['~strings'].Objects[FDefaultStrings.Names[i]].Strings[FSupportedLangs[j]]
          else
            jso.Strings[FSupportedLangs[j]] := '';

        if not FJSON.Objects['~strings'].Has[FDefaultStrings.Names[i]] then
          FJSON.Objects['~strings'].Objects[FDefaultStrings.Names[i]] := JSON();

        jso.Strings[DesignLang] := FDefaultStrings.ValueFromIndex[i];
        FJSON.Objects['~strings'].Objects[FDefaultStrings.Names[i]].Strings[DesignLang] := FDefaultStrings.ValueFromIndex[i];

        slFound.Add(FDefaultStrings.Names[i]);

        proc(FDefaultStrings.Names[i], jso);
      end;

      slFound.Sort;

      FJSON.Objects['~strings'].Each(
        procedure(const ID : string; const jso : IJSONObject)
        begin
          if slFound.IndexOf(ID) < 0 then
          begin
            proc(ID, jso);
          end;
        end
      );
    finally
      slFound.Free;
    end;
  end;
end;

procedure TLangManager.Edit;
begin
  if Assigned(FEditor) then
    FEditor.Edit(Self)
  else
    raise Exception.Create('No Editor Class Registered.');
end;

procedure TLangManager.EnsureLoaded;
begin
  if not (csLoading in ComponentState) then
  begin
    if not FLoaded then
      Loaded;
  end else
    Abort;
end;

function TLangManager.GetProperty(cmp: TComponent; const prop: string): string;
var
  rtti : TRttiContext;
  rttiType : TRttiType;
  rttiProp : TRttiProperty;
begin
  EnsureLoaded;
  if not FJSON.Has[cmp.Name] then
    FJSON.Objects[cmp.Name] := JSON;
  if not FJSON.Objects[cmp.Name].Has[prop] then
    FJSON.Objects[cmp.Name].Objects[prop] := JSON;
  if not FJSON.Objects[cmp.Name].Objects[prop].Has[SelectedLang] then
  begin
    rttiType := rtti.GetType(cmp.ClassType);
    rttiProp := rttiType.GetProperty(prop);
    Result := rttiProp.GetValue(cmp).AsType<String>;
  end else
    Result := FJSON.Objects[cmp.Name].Objects[prop].Strings[SelectedLang];
end;

function TLangManager.GetString(const id: string): string;
begin
  EnsureLoaded;
  if not FJSON.Has['~strings'] then
    FJSON.Objects['~strings'] := JSON;
  if not FJSON.Objects['~strings'].Has[id] then
    FJSON.Objects['~strings'].Objects[id] := JSON;
  if FJSON.Objects['~strings'].Objects[id].Has[SelectedLang] then
    Result := FJSON.Objects['~strings'].Objects[id].Strings[SelectedLang]
  else if FJSON.Objects['~strings'].Objects[id].Has[DesignLang] then
    Result := FJSON.Objects['~strings'].Objects[id].Strings[DesignLang]
  else if FDefaultStrings.IndexOfName(id) >= 0 then
    Result := FDefaultStrings.Values[id]
  else
    Result := '';
end;

function TLangManager.GetStringValues(const id: string): IJSONObject;
begin
  EnsureLoaded;
  if FJSON.Has['~strings'] and FJSON.Objects['~strings'].Has[id] then
    Result := FJSON.Objects['~strings'].Objects[id]
  else
    Result := JSON();
end;

function TLangManager.GetValues(cmp: TComponent;
  const prop: string): IJSONObject;
begin
  EnsureLoaded;
  if FJSON.Has[cmp.Name] and FJSON.Objects[cmp.Name].Has[prop] then
    Result := FJSON.Objects[cmp.Name].Objects[prop]
  else
    Result := JSON();
end;

procedure TLangManager.Loaded;
begin
  if FLoaded then
    exit;
  inherited;
  FLoaded := True;
  FUpdatedLang := False;
  Refresh;
end;

procedure TLangManager.Refresh;
begin
  if Root <> nil then
  begin
    FJSONFilename := TPath.Combine(FDataFolder, ApplicationName+'.'+Root.ClassName+'.json');
    if TFile.Exists(FJSONFilename) then
    begin
      FJSON := JSON.LoadFromFile(FJSONFilename);
      if FJSON.Has['~settings'] then
        FSettingsJSON := FJSON.Objects['~settings'];
      FJSON.Each(
        procedure(const Name : string; const Obj : IJSONObject)
        begin
          if Obj.SameAs(FSettingsJSON) then
            exit;
          Obj.Each(
            procedure(const Name : string; const Prop : IJSONObject)
            begin
              Prop.Each(
                procedure(const Lang : string; const Value : string)
                begin
                  if FSupportedLangs.IndexOf(Lang) < 0 then
                    FSupportedLangs.Add(Lang);
                end
              );
            end
          );
        end
      );
    end else
      FJSON := JSON;
  end;
  UpdateRootProperties;
end;

class procedure TLangManager.SetSelectedLang(const Value: string);
var
  i: Integer;
begin
  if Value = '' then
    exit;

  FSelectedlang := Value;

  TMonitor.Enter(FLangs);
  try
    for i := 0 to FLangs.Count-1 do
      FLangs[i].UpdateRootProperties;
  finally
    TMonitor.Exit(FLangs);
  end;
end;

procedure TLangManager.SaveFile;
begin
  EnsureLoaded;
  FJSON.Objects['~settings'] := FSettingsJSON;
  FJSON.SaveToFile(FJSONFilename,TWhitespace.standard);
end;

procedure TLangManager.SetDefaultStrings(const Value: TDefaultStrings);
begin
  FDefaultStrings.Assign(Value);
end;

class procedure TLangManager.SetDesignLang(const Value: string);
var
  i : integer;
begin
  if Value = '' then
    exit;

  FDesignLang := Value;

  TMonitor.Enter(FLangs);
  try
    for i := 0 to FLangs.Count-1 do
      FLangs[i].UpdateRootProperties;
  finally
    TMonitor.Exit(FLangs);
  end;
end;

procedure TLangManager.SetProperty(cmp: TComponent; const prop: string;
  const Value: string);
begin
  EnsureLoaded;
  FUpdatedLang := True;
  if not FJSON.Has[cmp.Name] then
    FJSON.Objects[cmp.Name] := JSON;
  if not FJSON.Objects[cmp.Name].Has[prop] then
    FJSON.Objects[cmp.Name].Objects[prop] := JSON;
  FJSON.Objects[cmp.Name].Objects[prop].Strings[SelectedLang] := Value;

  UpdateRootProperties;
end;

procedure TLangManager.SetRoot(const Value: TComponent);
begin
  FRoot := Value;
  Refresh;
end;

procedure TLangManager.SetString(const id: string; const Value: string);
begin
  EnsureLoaded;
  FUpdatedLang := True;
  if not FJSON.Has['~strings'] then
    FJSON.Objects['~strings'] := JSON;
  if not FJSON.Objects['~strings'].Has[id] then
    FJSON.Objects['~strings'].Objects[id] := JSON;

  FJSON.Objects['~strings'].Objects[id].Strings[SelectedLang] := Value;

  UpdateRootProperties;
end;

procedure TLangManager.SetStringValues(const id: string;
  const Value: IJSONObject);
begin
  EnsureLoaded;
  if not FJSON.Has['~strings'] then
    FJSON.Objects['~strings'] := JSON;
  FJSON.Objects['~strings'].Objects[id] := Value;
end;

procedure TLangManager.SetSupportedLangs(const Value: TStrings);
var
  i: Integer;
begin
  FSupportedLangs.Assign(Value);
  for i := FSupportedLangs.Count - 1 downto 0 do
    if FSupportedLangs[i].Trim = '' then
      FSupportedLangs.Delete(i);
end;

procedure TLangManager.SetValues(cmp: TComponent; const prop: string;
  const Value: IJSONObject);
begin
  EnsureLoaded;
  if not FJSON.Has[cmp.Name] then
    FJSON.Objects[cmp.Name] := JSON();

  FJSON.Objects[cmp.Name].Objects[prop] := Value;
end;

function TLangManager.ApplicationName: string;
begin
  Result := ChangeFileExt(ExtractFileName(ParamStr(0)),'');
end;

class constructor TLangManager.Create;
begin
  FLangs := TList<TLangManager>.Create;
  SelectedLang := 'English';
  DesignLang := 'English';
end;

constructor TLangManager.Create(ARoot: TComponent; const AName: string);
begin
  Create(nil);
  FRoot := ARoot;
  Name := AName;
end;

end.
